<!DOCTYPE html>
<html lang="en">
<head>
    <title>Super Digit</title>
</head>
    <body>

        <?php

        class Digit{
            public $values;
            public $sum = 0;

            function superDigit($value){
                echo "Sum of each digits till single digit ", $value, " is: ";
                while($value > 0 || $sum > 9) {
                    if($value == 0) {
                       $value = $sum;
                       $sum = 0;
                    }
                    $sum = $sum + $value % 10;
                    $value = $value/10;
                 }
                 echo  $sum, "<br>";
            }

        }

        $digit = new Digit();
        $digit->superDigit("18");
        $digit->superDigit("4");
        $digit->superDigit("258");         

        ?>
    </body>
</html>