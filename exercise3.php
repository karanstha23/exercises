<!DOCTYPE html>
<html lang="en">
<head>
    <title>String Merge</title>
</head>
    <body>

        <?php

            $str1 = "MICHAEL";
            $str2 = "JORDAN";
            $merge = "";

            Class Combine {
                public $str1, $str2;

                public function stringMerge($string1, $string2){
                    for($i = 0; $i < strlen($string1) || $i < strlen($string2); $i++){
                        if($i < strlen($string1)){
                            $merge = $merge.$string1[$i];
                        }
                        if($i < strlen($string2)){
                            $merge = $merge.$string2[$i];
                        }
                        
                    }
                    echo "String1: ", $string1, "<br>", "String2: ", $string2, "<br>", "String Merge Alternatively: ", $merge;
                }
            }

            $combine = new Combine();
            $combine -> stringMerge("ABC", "CDE");    
        ?>
    </body>
</html>