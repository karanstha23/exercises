<!DOCTYPE html>
<html lang="en">
<head>
    <title>Words without any repeating letters</title>
</head>
    <body>

        <?php

            $str = ["documentarily", "aftershock", "six-year-old", "Euclidean", "Double-down"];

            function nonRepeatingLetters($str)
            {
                $str = preg_replace('/[0-9\@\.\-\" "]+/', '', $str);

                for($i = 0; $i < strlen($str); $i++)
                {
                    for($j = $i + 1; $j < strlen($str); $j++)
                    {
                        if($str[$i] == $str[$j])
                        {
                            return false;
                        }
                    }
                }
                
                return true;
            }

            for($i = 0; $i < sizeof($str); $i++){
                if(nonRepeatingLetters(strtolower($str[$i])))
                    {
                        echo $str[$i], ": TRUE";
                        echo "<br>";
                    }
                    else
                    {
                        echo $str[$i], ": FALSE";
                        echo "<br>";
                    }
            }

        ?>
    </body>
</html>